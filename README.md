# A Sample of [t-ceremony](https://bitbucket.org/imagire/t-ceremony): A Continuous Publishing system #

### What is this repository for? ###

* This sample use a continuous publishing system t-ceremony. When Re:VIEW files are committed in a Bitbucket repository, CI makes a complied pdf and ships to another Bitbucket repository.

### Who do I talk to? ###

* Repo owner: Takashi Imagire

